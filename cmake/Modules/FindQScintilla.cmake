# Locate QScintilla
# This module defines
# QSCINTILLA_LIBRARY
# QSCINTILLA_FOUND, if false, do not try to link to QScintilla 
# QSCINTILLA_INCLUDE_DIR, where to find the headers
#
# Modified from OpenAL.cmake file of the CMake distribution

# Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
#
# Contact: Matti Airas <matti.p.airas@nokia.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# version 2 as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
# 02110-1301 USA

INCLUDE(FindQt4)

FIND_PATH(QSCINTILLA_INCLUDE_DIR qsciscintilla.h
  HINTS
  $ENV{QSCINTILLADIR}
  PATH_SUFFIXES include/qt4/Qsci include/qt4 include
  PATHS
  ~/Library/Frameworks
  /Library/Frameworks
  /usr/local
  /usr
  /sw # Fink
  /opt/local # DarwinPorts
  /opt/csw # Blastwave
  /opt
)

FIND_LIBRARY(QSCINTILLA_LIBRARY 
  NAMES QScintilla2 qscintilla2
  HINTS
  $ENV{QSCINTILLADIR}
  PATH_SUFFIXES lib64 lib libs64 libs libs/Win32 libs/Win64
  PATHS
  ~/Library/Frameworks
  /Library/Frameworks
  /usr/local
  /usr
  /sw
  /opt/local
  /opt/csw
  /opt
)


SET(QSCINTILLA_FOUND "NO")
IF(QSCINTILLA_LIBRARY AND QSCINTILLA_INCLUDE_DIR)
  SET(QSCINTILLA_FOUND "YES")
ENDIF(QSCINTILLA_LIBRARY AND QSCINTILLA_INCLUDE_DIR)
